import React, { useState, useEffect } from 'react';
import bridge from '@vkontakte/vk-bridge';
import { View, ScreenSpinner, AdaptivityProvider, AppRoot, ConfigProvider, SplitLayout, SplitCol, Panel, Group, FormLayout } from '@vkontakte/vkui';

import '@vkontakte/vkui/dist/vkui.css';
import HelloWindow from './components/HelloWindow/HelloWindow';
import AddGroup from './components/AddGroup/AddGroup';
import axios from 'axios';



const App = () => {
	const [scheme, setScheme] = useState('bright_light')
	const [activePanel, setActivePanel] = useState('helloPage');
	const [fetchedUser, setUser] = useState(null);
	const [popout, setPopout] = useState(<ScreenSpinner size='large' />);
	const [fetchedUserGroups, setFetchedUserGroups] = useState([])
	let fetchUserList = []
	const [childTasks, setChildTasks] = useState([])

	useEffect(() => {
		bridge.subscribe(({ detail: { type, data }}) => {
			if (type === 'VKWebAppUpdateConfig') {
				setScheme(data.scheme)
			}
		});

		async function fetchData() {
			const user = await bridge.send('VKWebAppGetUserInfo');
			setUser(user);
			setPopout(null);
			const response = await axios.get('https://dev.catfinity.ru/group/' + user.id);
			// console.log(response.data);
			fetchUserList = response.data
			setFetchedUserGroups(response.data);
		}
		fetchData();
	}, []);

	return (
		<ConfigProvider scheme={scheme}>
			<AdaptivityProvider>
				<AppRoot>
					<SplitLayout popout={popout}>
						<SplitCol>
							<View activePanel={activePanel}>
								<Panel id='helloPage'>
									<HelloWindow fetchedUser={fetchedUser} userGroups={fetchedUserGroups} setActivePanel={setActivePanel} />
								</Panel>
								<Panel id='addGroup'>
									<AddGroup fetchedUser={fetchedUser} setActivePanel={setActivePanel}/>
								</Panel>
								<Panel id='mainApp'>
									<AddGroup fetchedUser={fetchedUser} setActivePanel={setActivePanel}/>
								</Panel>
							</View>
						</SplitCol>
					</SplitLayout>
				</AppRoot>
			</AdaptivityProvider>
		</ConfigProvider>
	);
}

export default App;
