import { Group } from "@vkontakte/vkui"
import ButtonDownGroup from "./components/ButtonDownGroup"
import Header from "./components/Header"
import Payload from "./components/Payload"

const AddGroup = (props)=>{
    return(<>
        <Group>
            <Header setActivePanel={props.setActivePanel} />
            <Payload fetchedUser={props.fetchedUser} setActivePanel={props.setActivePanel} />
        </Group>
    </>)
}

export default AddGroup