import {ButtonGroup, Button } from '@vkontakte/vkui';
import axios from 'axios';
import { useState } from 'react';

const ButtonDownGroup = (props)=>{
    const [submitBtnValue, setSubmitBtnValue] = useState("Добавить")
    const buttonGroupHighlightStyles = {
        boxSizing: "border-box",
        justifyContent: "end",
        padding: "1em"
        };
        
    return(
        <ButtonGroup
            mode="horizontal"
            gap="m"
            stretched
            style={buttonGroupHighlightStyles}
        >
            <Button 
                size="l" 
                appearance="secondary"
                stretched
                onClick={()=>{
                    props.setActivePanel("helloPage")
                }}
            >
                Отмена
            </Button>
            <Button 
                size="l" 
                appearance="accent"
                stretched
                onClick={()=>{
                    if(props?.groupName && props?.organisationName) {
                        let temp = props.groupName + " (" + props.organisationName 
                        if (props?.learnGroup){
                            temp = temp + ", " + props.learnGroup + ")"
                        } else {
                            temp = temp + ")"
                        }
                        console.log(temp);
                        axios.post("https://dev.catfinity.ru/group/" + props.fetchedUser.id, JSON.stringify({"name": temp}) ).then(()=>{
                            // props.setActivePanel("helloPage")
                            location.reload();
                        })
                    } else {
                        setSubmitBtnValue("Вы заполнили не все поля!")
                        let temp = props.groupName + " (" + props.organisationName 
                        temp = temp + ", " + props.learnGroup + ")"
                        console.log(temp);
                        setTimeout(()=>{
                            setSubmitBtnValue("Добавить")
                        }, 1300)
                    }
                }}
            >
                {submitBtnValue}
            </Button>
        </ButtonGroup>
    )

}

export default ButtonDownGroup