import React, { useState} from 'react';

import { 
    PanelHeader, 
    ButtonGroup,
    Button
} from '@vkontakte/vkui';


import { Icon16ChevronLeft } from '@vkontakte/icons';

const Header = (props)=>{
    return(
        <PanelHeader>
            <ButtonGroup
                mode="horizontal"
                gap="m"
                stretched
                style={{
                    boxSizing: "border-box",
                    display: "flex",
                    justifyContent:"space-between",
                    padding: "1em"
                  }}
            >
                
                    <Button
                        appearance="secondary"
                        onClick={props.setActivePanel('helloPage')}
                    >

                        <Icon16ChevronLeft />
                    </Button>
                
                <div>
                    Стартовый экран
                </div>
                <div></div>
            </ButtonGroup>
        </PanelHeader>
    )
}

export default Header