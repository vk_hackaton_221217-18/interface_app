import {FormItem, FormLayout, Group, Input} from '@vkontakte/vkui';
import { useState } from 'react';
import ButtonDownGroup from './ButtonDownGroup';

const Payload = (props)=>{
    const [groupName, setGroupName] = useState("")
    const [organisationName, setOrganisationName] = useState("")
    const [learnGroup, setLearnGroup] = useState("")
    return(
        <>
            <FormLayout>
                <FormItem
                    top="Название группы"
                >
                <Input
                    name="groupName"
                    value={groupName}
                    onChange={(e)=>{setGroupName(e.target.value)}}
                />
                </FormItem>
                <FormItem
                    top="Учебное заведение"
                >
                    <Input
                    name="organisationName"
                        value={organisationName}
                        onChange={(e)=>{setOrganisationName(e.target.value)}}
                    />
                </FormItem>
                <FormItem
                    top="Группа(класс)"
                >
                    <Input
                    name="learnGroup"
                        value={learnGroup}
                        onChange={(e)=>{setLearnGroup(e.target.value)}}
                    />
                </FormItem>
            </FormLayout>
            <ButtonDownGroup fetchedUser={props.fetchedUser} groupName={groupName} organisationName={organisationName} learnGroup={learnGroup} setActivePanel={props.setActivePanel}/>
        </>
        )
    }

export default Payload