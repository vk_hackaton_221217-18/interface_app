import { Icon16ChevronLeft } from "@vkontakte/icons"
import { Button, ButtonGroup, PanelHeader } from "@vkontakte/vkui"

const Header = (props)=>{
    return(<>
        <PanelHeader>
            <ButtonGroup
                mode="horizontal"
                gap="m"
                stretched
                style={{
                    boxSizing: "border-box",
                    display: "flex",
                    justifyContent:"space-between",
                    padding: "1em"
                }}
            >
                <Button
                    appearance="secondary"
                    onClick={()=>{
                        props.setActivePanel("helloPage")
                    }}
                >
                    <Icon16ChevronLeft />
                </Button>
            </ButtonGroup>
        </PanelHeader>
    </>)
}

export default Header