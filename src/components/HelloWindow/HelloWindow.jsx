import { FixedLayout, FormItem, FormLayout, Group, PanelHeader, Select } from "@vkontakte/vkui"
import axios from "axios"
import { useEffect, useState } from "react"
import ButtonInput from "./components/ButtonInput"

const HelloWindow = (props)=>{
    const [groupList, setGroupList] = useState([])

    let list = []

    return(
        <Group>
            <FormLayout>
                <PanelHeader>Авторизация</PanelHeader>
                <FormItem top="Выберите дневник">
                    {
                        props.userGroups.map((e)=>{
                            list.push(
                                {"value": e.id, "label": e.name}
                            ) 
                        })
                    }
                    <Select 
                        placeholder="Кружок по вязанию (Дом пионеров 1, 12группа)"
                        options={list}
                        onChange={(e)=>{
                            let yourDate = new Date()
                            yourDate = yourDate.toISOString().split('T')[0]
                            axios.post("https://dev.catfinity.ru/task_by_day/" + e.target.value, JSON.stringify({"user_id": props.fetchedUser.id, "date":  yourDate})).then((e)=>{

                            })
                            console.log(e.target.value);
                        }}
                    />
                </FormItem>
                <ButtonInput setActivePanel={props.setActivePanel}/>
            </FormLayout>
        </Group>
    )
}

export default HelloWindow