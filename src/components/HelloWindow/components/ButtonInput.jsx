
import {ButtonGroup, Button } from '@vkontakte/vkui';


const ButtonInput = (props)=>{

    const buttonGroupHighlightStyles = {
        boxSizing: "border-box",
        justifyContent: "end",
        padding: "1em"
      };

    return(
        <ButtonGroup
            mode="horizontal"
            gap="m"
            stretched
            style={buttonGroupHighlightStyles}
        >
            <Button 
                size="l" 
                appearance="secondary"
                onClick={()=>{props.setActivePanel("mainApp")}}
                
            >
                Настройки
            </Button>
            <Button 
                size="l" 
                appearance="secondary"
                onClick={()=>{props.setActivePanel("addGroup")}}
            >
                Новый дневник
            </Button>
            <Button 
                size="l" 
                appearance="accent"
                stretched
            >
                Вход
            </Button>
        </ButtonGroup>
    )
}

export default ButtonInput