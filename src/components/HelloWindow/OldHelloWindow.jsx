import { useState, useEffect } from 'react';
import bridge from '@vkontakte/vk-bridge';
import { Group, FormLayout, FixedLayout, PanelHeader } from '@vkontakte/vkui';

import {FormItem, Select} from '@vkontakte/vkui';
import Button_input from './components/ButtonInput';

import '@vkontakte/vkui/dist/vkui.css';

const HelloWindow = (props)=>{
    const [scheme, setScheme] = useState('bright_light')

    const buttonGroupHighlightStyles = {
        border: "2px dotted tomato",
        boxSizing: "border-box",
    };

	useEffect(() => {
		bridge.subscribe(({ detail: { type, data }}) => {
			if (type === 'VKWebAppUpdateConfig') {
				setScheme(data.scheme)
			}
		});
	}, []);

    return(
        <>
            <h2>HelloWindow</h2>
            <button
                onClick={()=>{props.setActivePanel("addGroup")}}
            >addGroup</button>
        </>
        // <Group>
        //     <FormLayout>
        //         <PanelHeader>Авторизация</PanelHeader>
        //         <FormItem top="Выберите дневник">
        //            <Select
        //                placeholder="Кружок по вязанию (Дом пионеров 1, 12группа)"
        //                options={[
        //                 {
        //                     value: "Программирование на Python (Кванториум, IT-12)",
        //                     label: "Программирование на Python (Кванториум, IT-12)",
        //                 },
        //                 {
        //                     value: "Математика (СОШ 17, 11Б)",
        //                     label: "Математика (СОШ 17, 11Б)",
        //                 },
        //                ]}
        //             />
        //         </FormItem>
        //         <Button_input setActivePanel={props.setActivePanel("addGroup")}/>
        //     </FormLayout>
        //     <FixedLayout vertical='bottom' style={{display:"flex", justifyContent: "end"}}>
        //     </FixedLayout>
        // </Group>
    )
}

export default HelloWindow